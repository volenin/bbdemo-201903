package com.artilekt.account.business;

import org.junit.Test;

import static org.junit.Assert.*;

public class OverdraftAccountTest {

    @Test
    public void whenOverdrawnWithinLimit_thenSuccess() {
        BaseAccount account = new OverdraftAccount(1000);
        account.withdraw(1200);
        assertEquals(-200, account.getBalance());
    }

}