package com.artilekt.account;

import com.artilekt.account.business.Account;
import com.artilekt.account.business.BaseAccount;
import org.junit.Test;

import static org.junit.Assert.*;

public class AccountTest {
    private Account ZERO_BALANCE_ACCOUNT = new Account(0);
    private Account POSITIVE_BALANCE_ACCOUNT = new Account(1000);

    @Test
    public void whenDepositPositiveAmount_thenSuccess() {
        // init
        Account account = new Account(1000);

        // condition
        account.deposit(20);

        // validation
        assertEquals(1020, account.getBalance());
    }

    @Test(expected = IllegalArgumentException.class)
    public void depositNegativeAmountToAccountWithInitialBalance_thenError() {
        POSITIVE_BALANCE_ACCOUNT.deposit(-50);
    }

    @Test(expected = IllegalArgumentException.class)
    public void withdrawNegativeAmountFromAccountWithInitialBalance_thenError() {
        POSITIVE_BALANCE_ACCOUNT.withdraw(-50);
    }


    @Test(expected = IllegalArgumentException.class)
    public void whenOverdrawAccount_thenError() {
        POSITIVE_BALANCE_ACCOUNT.withdraw(1050);
    }



}