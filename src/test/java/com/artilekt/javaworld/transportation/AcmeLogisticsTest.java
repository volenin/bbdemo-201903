package com.artilekt.javaworld.transportation;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AcmeLogisticsTest {
    private AcmeLogistics acmeLogistics = new AcmeLogistics();
    private DeliveryVehicle DRONE_1 = new Drone("drone1");
    private DeliveryVehicle DRONE_2 = new Drone("drone2");
    private DeliveryVehicle TRUCK_1 = new Truck("truck1");
    private DeliveryVehicle TRUCK_2 = new Truck("truck2");
    private DeliveryVehicle BOAT_1 = new Boat("boat1");
    private DeliveryVehicle BOAT_2 = new Boat("boat2");



    @Before
    public void setup() {
        acmeLogistics.addToFleet(DRONE_1);
        acmeLogistics.addToFleet(DRONE_2);
        acmeLogistics.addToFleet(TRUCK_1);
        acmeLogistics.addToFleet(TRUCK_2);
        acmeLogistics.addToFleet(BOAT_1 );
        acmeLogistics.addToFleet(BOAT_2 );
    }

//    @Test
    public void logisticsContract() {
        AcmeLogistics acmeLogistics = new AcmeLogistics();
        DeliveryVehicle drone = new Drone("drone1");
        DeliveryVehicle truck = new Truck("truck1");
        DeliveryVehicle boat = new Boat("boat1");

        acmeLogistics.planRoute(drone);
        acmeLogistics.addToFleet(drone);
        acmeLogistics.addToFleet(truck);
        acmeLogistics.addToFleet(boat);
    }

    @Test
    public void whenAddVehicles_thenSuccess() {
        assertTrue(acmeLogistics.getFleetSize() > 5);
    }

    @Test
    public void whenListVehicles_thenSuccess() {
        assertEquals(6, acmeLogistics.getFleetSize());
    }

    @Test(expected = UnsupportedOperationException.class)
    public void whenUnauthorizedModification_thenFail() {
        List<DeliveryVehicle> deliveryVehicles = acmeLogistics.listVehicles();
        deliveryVehicles.add(new Drone("zzz"));
    }

    @Test
    public void whenCheckForVehicle_thenSuccess() {
        DeliveryVehicle DRONE_1_CLONE = new Drone("drone1");
        assertTrue(acmeLogistics.isPartOfFleet(DRONE_1));
        assertTrue(acmeLogistics.isPartOfFleet(DRONE_1_CLONE));
    }

}