package com.artilekt.javaworld;

import com.artilekt.javaworld.people.CanadianCitizen;
import com.artilekt.javaworld.people.Gender;
import com.artilekt.javaworld.people.Person;
import com.artilekt.javaworld.people.UsaCitizen;
import org.junit.Test;

public class PersonTest {

    @Test
    public void demoPersonPrintout() {
        Person p = new Person(30, "paradise str", Gender.MALE, "Vlad", "Olenin");
        System.out.println(p);
    }

    @Test
    public void demoCitizensTest() {
        Person canadian = new CanadianCitizen(30, "paradise str", Gender.MALE, "Vlad", "Olenin", "sin123");
        Person american = new UsaCitizen(20, "usa street", Gender.FEMALE, "Jane", "Doe", "ssn123");

        System.out.println(canadian);
        System.out.println(american);

        System.out.println("Canadian Sin: "+((UsaCitizen) canadian).getSsn());
    }

}