package com.artilekt.javaworld;

import com.artilekt.javaworld.people.BorderGuard;
import com.artilekt.javaworld.people.CanadianCitizen;
import com.artilekt.javaworld.people.Gender;
import com.artilekt.javaworld.people.UsaCitizen;
import org.junit.Test;

public class BorderGuardTest {
    private CanadianCitizen canadian = new CanadianCitizen(30, "paradise str", Gender.MALE, "Vlad", "Olenin", "sin123");
    private UsaCitizen american = new UsaCitizen(20, "usa street", Gender.FEMALE, "Jane", "Doe", "ssn123");

    @Test
    public void validateIdDocumentAtTheBorder() {
        BorderGuard guard = new BorderGuard();


        guard.validateIdentity(canadian);
        guard.validateIdentity(american);
    }

}