package com.artilekt.account;

import com.artilekt.account.business.Account;
import com.artilekt.account.business.AccountOverdrawnException;
import com.artilekt.account.business.InvalidAmountException;

public class AccountCli {
    public static void main(String[] args) {
        System.out.println("First parameter is: "+args[0]);
        String initialBalanceStr = args[0];
        int bal = Integer.parseInt(initialBalanceStr);
        int withdrawAmount = Integer.parseInt(args[1]);

        Account account = new Account(bal);
        try {
            account.withdraw(withdrawAmount);
        }
        catch (InvalidAmountException IAe) {
            System.out.println("an error occured: "+IAe.getMessage());
 //           throw IAe;
        }
        catch (AccountOverdrawnException AOe) {
            System.out.println("You are not allowed to overdraw the account! Balance is: "
                    +account.getBalance()+", withdraw amount is: "+AOe.getWithdrawAmount());
        }


        System.out.println("SUCCESS");
    }
}
