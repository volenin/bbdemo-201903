package com.artilekt.account.business;

public abstract class BaseAccount {
    private int balance; //=0

    public BaseAccount() {
        System.out.println("...initializing...");
        this.balance = 0;
    }

    public BaseAccount(int balance) {
        this.balance = balance;
    }


    public void deposit(int amount) {
        validateAmount(amount);
        incBalanceBy(amount);
    }

    public int getBalance() {
        return balance;
    }

    public abstract void withdraw(int amount);



    protected void validateAmount(int amount) {
        if (amount < 0)  throw new InvalidAmountException("Amount can't be negative: "+amount);
    }

    protected void incBalanceBy(int amount) {
        this.balance += amount;
    }

    protected void decBalanceBy(int amount) {
        this.balance -= amount;
    }
}
