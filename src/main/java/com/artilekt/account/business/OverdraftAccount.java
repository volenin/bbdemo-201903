package com.artilekt.account.business;

public class OverdraftAccount extends BaseAccount {
    private final static int OVERDRAFT_LIMIT = 1000;

    public OverdraftAccount() {
    }

    public OverdraftAccount(int balance) {
        super(balance);
    }

    @Override
    public void withdraw(int amount) {
        validateAmount(amount);
        checkForOverdraft(amount);
        decBalanceBy(amount);
    }


    protected void checkForOverdraft(int amount) {
        if (amount > this.getBalance() + OVERDRAFT_LIMIT)     throw new AccountOverdrawnException("Can't overdraw account", amount);
    }

}
