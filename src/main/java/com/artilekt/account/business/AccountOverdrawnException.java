package com.artilekt.account.business;

public class AccountOverdrawnException extends RuntimeException {
    private int withdrawAmount;

    public AccountOverdrawnException() {
    }

    public AccountOverdrawnException(String message, int withdrawAmount) {
        super(message);
        this.withdrawAmount = withdrawAmount;
    }

    public AccountOverdrawnException(String message, Throwable cause) {
        super(message, cause);
    }

    public AccountOverdrawnException(Throwable cause) {
        super(cause);
    }

    public AccountOverdrawnException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public int getWithdrawAmount() {
        return withdrawAmount;
    }
}
