package com.artilekt.account.business;

public class Account {
    private int balance; //=0

    public Account() {
        System.out.println("...initializing...");
        this.balance = 0;
    }

    public Account(int balance) {
        this.balance = balance;
    }


    public void deposit(int amount) {
        if (amount < 0)   throw new InvalidAmountException("Amount can't be negative: "+amount);

        this.balance += amount;
    }

    public int getBalance() {
        return balance;
    }

    public void withdraw(int amount) {
        if (amount < 0)             throw new InvalidAmountException("Amount can't be negative: "+amount);
        if (amount > this.balance)  throw new AccountOverdrawnException("Can't overdraw account", amount);
        balance -= amount;
    }
}
