package com.artilekt.javaworld.transportation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AcmeLogistics {
    private List<DeliveryVehicle> deliveryVehicles = new ArrayList<>();

    public Route planRoute(DeliveryVehicle deliveryVehicle) {
        throw new UnsupportedOperationException();
    }

    public void addToFleet(DeliveryVehicle deliveryVehicle) {
        deliveryVehicles.add(deliveryVehicle);
    }

    public int getFleetSize() {
        return deliveryVehicles.size();
    }


    public List<DeliveryVehicle> listVehicles() {
        return Collections.unmodifiableList(deliveryVehicles);
    }

    public boolean isPartOfFleet(DeliveryVehicle deliveryVehicle) {
        return deliveryVehicles.contains(deliveryVehicle);
    }

}
