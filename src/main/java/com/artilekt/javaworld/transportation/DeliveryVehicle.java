package com.artilekt.javaworld.transportation;

import java.util.Objects;

public abstract class DeliveryVehicle {
    private String id;

    public DeliveryVehicle(String id) {
        this.id = id;
    }

    public abstract Coordinates getMyLocation();
    public abstract long calcTripDuration(Coordinates start, Coordinates finish);

/*    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return id.equals(((DeliveryVehicle)obj).id);
    }*/
// vech.get(4).equals(vehTOCompare)
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeliveryVehicle that = (DeliveryVehicle) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
