package com.artilekt.javaworld.transportation;

public class Boat extends DeliveryVehicle {
    public Boat(String id) {
        super(id);
    }

    @Override
    public Coordinates getMyLocation() {
        return null;
    }

    @Override
    public long calcTripDuration(Coordinates start, Coordinates finish) {
        return 0;
    }
}
