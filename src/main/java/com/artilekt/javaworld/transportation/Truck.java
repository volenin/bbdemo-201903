package com.artilekt.javaworld.transportation;

public class Truck extends DeliveryVehicle {
    public Truck(String id) {
        super(id);
    }

    @Override
    public Coordinates getMyLocation() {
        return null;
    }

    @Override
    public long calcTripDuration(Coordinates start, Coordinates finish) {
        return 0;
    }
}
