package com.artilekt.javaworld.people;

public class Refugee extends Person {
    public Refugee(int age, String address, Gender gender, String firstName, String lastName) {
        super(age, address, gender, firstName, lastName);
    }
}
