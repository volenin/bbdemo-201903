package com.artilekt.javaworld.people;

public class UsaCitizen extends Person implements Identifiable {
    private String ssn;

    public UsaCitizen(int age, String address, Gender gender, String firstName, String lastName, String ssn) {
        super(age, address, gender, firstName, lastName);
        this.ssn = ssn;
    }

    public String getSsn() {
        return ssn;
    }

    @Override
    public String toString() {
        return super.toString()+ "; UsaCitizen{" +
                "ssn='" + ssn + '\'' +
                '}';
    }

    @Override
    public String getId() {
        return getSsn();
    }
}
