package com.artilekt.javaworld.people;

public enum Gender {
    MALE, FEMALE, UNDEFINED
}
