package com.artilekt.javaworld.people;

public class Person {
    private int age;
    private String address;
    private Gender gender;
    private String firstName;
    private String lastName;

    public int getAge() {
        return age;
    }

    public String getAddress() {
        return address;
    }

    public Gender getGender() {
        return gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Person(int age, String address, Gender gender, String firstName, String lastName) {
        this.age = age;
        this.address = address;
        this.gender = gender;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", address='" + address + '\'' +
                ", gender=" + gender +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}
