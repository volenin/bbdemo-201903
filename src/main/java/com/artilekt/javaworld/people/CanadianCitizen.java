package com.artilekt.javaworld.people;

public class CanadianCitizen extends Person implements Identifiable {
    private String sin;

    public CanadianCitizen(int age, String address, Gender gender, String firstName, String lastName, String sin) {
        super(age, address, gender, firstName, lastName);
        this.sin = sin;
    }

    public String getSin() {
        return sin;
    }

    @Override
    public String toString() {
        return super.toString()+"; CanadianCitizen{" +
                "sin='" + sin + '\'' +
                '}';
    }

    @Override
    public String getId() {
        return getSin();
    }
}
