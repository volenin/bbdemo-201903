package com.artilekt.javaworld.people;

public interface Identifiable {
    public String getId();
}
